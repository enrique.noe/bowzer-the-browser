﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="_Bowzer the Browser" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="icons" Type="Folder">
				<Item Name="message community.png" Type="Document" URL="../Source/_Bowzer the Browser/icons/message community.png"/>
				<Item Name="message private.png" Type="Document" URL="../Source/_Bowzer the Browser/icons/message private.png"/>
				<Item Name="message.png" Type="Document" URL="../Source/_Bowzer the Browser/icons/message.png"/>
				<Item Name="private.png" Type="Document" URL="../Source/_Bowzer the Browser/icons/private.png"/>
				<Item Name="UDClass.png" Type="Document" URL="../Source/_Bowzer the Browser/icons/UDClass.png"/>
				<Item Name="UDInterface.png" Type="Document" URL="../Source/_Bowzer the Browser/icons/UDInterface.png"/>
			</Item>
			<Item Name="support" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Actor List Actor.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Actor List Actor/Actor List Actor.lvlib"/>
				<Item Name="Actor List Incoming.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Actor List Actor/Actor List Incoming.lvlib"/>
				<Item Name="Actor List Outgoing.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Actor List Actor/Actor List Outgoing.lvlib"/>
				<Item Name="Bowzer Actor.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Bowzer Actor/Bowzer Actor.lvlib"/>
				<Item Name="Browser Base Actor.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Browser Base Actor/Browser Base Actor.lvlib"/>
				<Item Name="Choose Implementation Actor.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Choose Implementation Actor/Choose Implementation Actor.lvlib"/>
				<Item Name="Choose Implementation Incoming.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Choose Implementation Actor/Choose Implementation Incoming.lvlib"/>
				<Item Name="Choose Implementation Outgoing.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Choose Implementation Actor/Choose Implementation Outgoing.lvlib"/>
				<Item Name="List Base Actor.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/List Base Actor/List Base Actor.lvlib"/>
				<Item Name="Msg List Actor.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Msg List Actor/Msg List Actor.lvlib"/>
				<Item Name="Msg List Incoming.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Msg List Actor/Msg List Incoming.lvlib"/>
				<Item Name="Msg List Outgoing.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Msg List Actor/Msg List Outgoing.lvlib"/>
				<Item Name="Scripting Support.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Scripting Support/Scripting Support.lvlib"/>
				<Item Name="Search Results Actor.lvlib" Type="Library" URL="../Source/_Bowzer the Browser/support/Search Results Actor/Search Results Actor.lvlib"/>
				<Item Name="Bowzer (Same Project App Instance).vi" Type="VI" URL="../Source/_Bowzer the Browser/support/Bowzer (Same Project App Instance).vi"/>
			</Item>
		</Item>
		<Item Name="Images" Type="Folder">
			<Item Name="AF Guild Logo - Abbreviated (64x64).png" Type="Document" URL="../Images/AF Guild Logo - Abbreviated (64x64).png"/>
			<Item Name="Bowzer 500x500.png" Type="Document" URL="../Images/Bowzer 500x500.png"/>
			<Item Name="Zyah Logo (70x64).png" Type="Document" URL="../Images/Zyah Logo (70x64).png"/>
		</Item>
		<Item Name="Bowzer the Browser QD.vi" Type="VI" URL="../Source/Bowzer the Browser QD.vi"/>
		<Item Name="Bowzer the Browser.vi" Type="VI" URL="../Source/Bowzer the Browser.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Casting Utility For Actors.vim" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Actor/Casting Utility For Actors.vim"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Equal Comparable.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Equal/Equal Comparable/Equal Comparable.lvclass"/>
				<Item Name="Equal Functor.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Equal/Equal Functor/Equal Functor.lvclass"/>
				<Item Name="Equals.vim" Type="VI" URL="/&lt;vilib&gt;/Comparison/Equals.vim"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Is Class Not Interface Without Loading.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Is Class Not Interface Without Loading.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="QuickDrop Parse Plugin Variant.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/QuickDropSupport/QuickDrop Parse Plugin Variant.vi"/>
				<Item Name="QuickDrop Plugin Data ver1.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/QuickDropSupport/QuickDrop Plugin Data ver1.ctl"/>
				<Item Name="Read Class Ancestry Without Loading.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Read Class Ancestry Without Loading.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Search Unsorted 1D Array Core.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Helpers/Search Unsorted 1D Array Core.vim"/>
				<Item Name="Search Unsorted 1D Array.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Search Unsorted 1D Array.vim"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="TRef Traverse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse.vi"/>
				<Item Name="TRef TravTarget.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef TravTarget.ctl"/>
				<Item Name="VI Scripting - Traverse.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/traverseref.llb/VI Scripting - Traverse.lvlib"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
